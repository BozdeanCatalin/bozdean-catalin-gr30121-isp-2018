package bozdean.catalin.lab2.ex1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Maxim {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a: ");
		int a = Integer.parseInt(br.readLine());

		System.out.print("Enter b: ");
		int b = Integer.parseInt(br.readLine());
		Maxim(a, b);
	}

	private static void Maxim(int a, int b) {
		// TODO Auto-generated method stub
		if (a > b)
			System.out.println("Max is: " + a);
		else
			System.out.println("Max is: " + b);
	}

}
