package bozdean.catalin.lab3.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestCircle {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Radius=");
        int r = Integer.parseInt(br.readLine());
        System.out.print("Color=");
        String c = br.readLine();
        Circle c1=new Circle(r,c);
        c1.getRadius();
        c1.getArea();
	}

}
