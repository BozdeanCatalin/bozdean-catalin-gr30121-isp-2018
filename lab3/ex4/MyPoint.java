package bozdean.catalin.lab3.ex4;

public class MyPoint {
	int x;
 int y;
	
	public MyPoint(){
		x=0;
		y=0;
	}
	
	public MyPoint(int X,int Y){
		x=X;
		y=Y;
	}
	
	public void getX(){
		System.out.println("\nx= "+x);
	}
	
	public void getY(){
		System.out.println("\ny= "+y);
	}
	
	public void setX(int X){
		x=X;
	}
	
	public void setY(int Y){
		y=Y;
	}
	
	public void setXY(int X,int Y){
		x=X;
		y=Y;
	}
	
	public String toString(){
		System.out.print("\n("+x+","+y+")");
		return null;
	}
	
	public double distance(int X, int Y){
		double result=Math.sqrt((x-X)*(x-X)+(y-Y)*(y-Y));
		return result;
	}
	
	public double distance(MyPoint a) {
        double result=Math.sqrt((this.x-a.x)*(this.x-a.x)+(this.y-a.y)*(this.y-a.y));
		return result;
    }
}




