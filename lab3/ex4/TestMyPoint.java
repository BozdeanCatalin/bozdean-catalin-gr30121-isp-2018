package bozdean.catalin.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(3, 5);
        MyPoint p3 = new MyPoint(0, 2);
        p1.toString();
        p2.toString();
        p3.toString();
        p1.setX(2);
        p1.setY(7);
        p1.getX();
        p1.getY();
        p1.toString();
        p3.setXY(5, 5);
        p3.toString();
        p2.distance(0, 0);
        System.out.print("\nDistance is: "+p2.distance(p1));
        System.out.print("\nDistance is: "+p2.distance(p3));
    }
}
