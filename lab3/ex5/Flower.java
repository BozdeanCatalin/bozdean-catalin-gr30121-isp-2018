package bozdean.catalin.lab3.ex5;

public class Flower {
        int petal;
		static int nr = 0;
        Flower(){ 
        	System.out.println("Flower has been created!");
        	nr++;
        }
        
        public static void nrFlori(){
        	System.out.print("there are "+nr+" flowers created");
        }

        public static void main(String[] args) {
		Flower[] garden = new Flower[5];
		for(int i =0;i<5;i++){
			Flower f = new Flower();
			garden[i] = f;
		}
		nrFlori();
	}
}

