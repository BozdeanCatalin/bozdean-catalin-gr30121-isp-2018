package bozdean.catalin.lab10.ex3;

public class TwoCounters {
	public static void main(String[] args) {
		Counter1 c1=new Counter1("c1",null);
		Counter2 c2=new Counter2("c2",c1);
		c1.run();
		c2.run();
	}
	
}

class Counter1 extends Thread{
	Thread t1;
	
	Counter1(String name,Thread t){
        super(name);this.t1=t;
  }
	
	public void run(){
        for(int i=0;i<100;i++){
              System.out.println(getName() + " i = "+i);
              try {
            	  if (t1!=null) //t1.join();
                    Thread.sleep(10);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
        System.out.println(getName() + " job finalised.");
  }
}

class Counter2 extends Thread{
	Thread t2;
	
	Counter2(String name,Thread t){
        super(name);this.t2=t;
  }

  public void run(){
	  for(int i=100;i<200;i++){
              System.out.println(getName() + " i = "+i);
              try {
            	  if (t2!=null) //t2.join();
                    Thread.sleep(10);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
        System.out.println(getName() + " 2nd done.");
  }
}
