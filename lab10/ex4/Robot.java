package bozdean.catalin.lab10.ex4;

public class Robot extends Thread {

    private int id;
    private int x;
    private int y;
    private boolean alive;

    public Robot(int id, int x,int y){
        this.id=id;
        this.x=x;
        this.y=y;
        this.alive=true;
    }

    public boolean isiAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


}
