package bozdean.catalin.lab4.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Author {
	private static String name;
	private static String mail;
	private static char gender;
	
	public Author(String n,String m,char g){
		name=n;
		mail=m;
		gender=g;
	}
	
	public void getName(){
		System.out.print("The Name is: "+name);
	}

	public void setMail() throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Mail=");
        String m = br.readLine();
        mail=m;
}
	public void getMail(){
		System.out.print("The Mail is: "+mail);
}
	public void getGender(){
		System.out.print("The Gender is: "+gender);
}
	public String toString(){
		System.out.print(name+" ("+gender+") at "+mail);
		return null;
	}
}
