package bozdean.catalin.lab4.ex3;

import bozdean.catalin.lab4.ex2.Author;

public class Book {
	static String name;
	static Author author;
	static double price;
	static int qtyInStock;
	
	public Book (String n, Author a, double p) {
		name=n;
		author=a;
		price=p;
	}
	
	public Book (String n, Author a, double p, int q) {
		name=n;
		author=a;
		price=p;
		qtyInStock=q;
	}
	
	public static String getName(){
		return name;
	}
	
	public static Author getAuthor(){
		return author;
	}
	
	public static double getPrice(){
		return price;
	}
	
	public static void setPrice(double p){
		price=p;
	}
	
	public static int getQtyInStock(){
		return qtyInStock;
	}
	
	public static void setQtyInStock(int q){
		qtyInStock=q;
	}
	
	public String toString(){
		return "book-"+name+" by author-"+author.toString()+"\n";
	}
}
