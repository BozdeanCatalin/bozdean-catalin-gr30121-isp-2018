package bozdean.catalin.lab4.ex5;

public class cylinder extends Circle {
	double height=1.0;
    public cylinder() {};
    public cylinder(double radius)
    {
        super(radius);
    }
    public cylinder(double r, double h)
    {
        super(r);
        this.height=h;
    }

    public double getHeight() {
        return height;
    }

    public double getArea() {
    	System.out.print("Cylinder's area is: ");
        return 2*Math.PI*super.getRadius()*(height+super.getRadius());
    }
}
