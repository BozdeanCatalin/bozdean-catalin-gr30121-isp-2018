package bozdean.catalin.lab4.ex1;

public class Circle {
	private static double radius=1.0;
	private static String color="red";
	
	Circle(){}
	Circle(double r,String c){
		radius=r;
		color=c;
	}
	
	public static void getRadius(){
		System.out.print("The Radius is: "+radius);
	}
	
	public static void getArea(){
		System.out.print("The Area is: "+(3.14*radius*radius));
	}
}
