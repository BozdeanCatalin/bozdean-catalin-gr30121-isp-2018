package bozdean.catalin.lab4.ex6;

public class Test {
	public static void main(String[] args) {
        Circle c1 = new Circle(3.12, "green", true);
        Circle c2 = new Circle(4.7, "yellow", false);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        c1.tooString();
        c2.tooString();

        System.out.println();

        Rectangle r1 = new Rectangle(10.0, 10.0, "red", true);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        r1.toStringR();

        System.out.println();

        Square s1 = new Square(5.0, "purple", false);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        s1.toStringR();

        System.out.println();

        Square s2 = new Square();
        s2.setLength(2.0);
        s2.setWidth(2.0);
        System.out.println(s2.getArea());
        System.out.println(s2.getPerimeter());
    }
}
