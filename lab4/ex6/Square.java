package bozdean.catalin.lab4.ex6;

public class Square extends Rectangle {
	double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double side) {
        super.width = side;
    }

    public void setLength(double side) {
        super.length = side;
    }
    public void toStringR(){
        System.out.print("A Square with side = "+this.width+" which is a subclass of ");
        super.toooString();
    }
}
