package bozdean.catalin.lab4.ex6;

public class Shape {
	String color;
    boolean filled;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void Filled(boolean filled) {
        this.filled = filled;
    }


    public void toooString() {
        if(this.filled==true)
            System.out.println("A Shape with color " + this.color + " and its filled");
        if(this.filled==false)
            System.out.println("A Shape with color " + this.color + " and its NOT filled");
    }
}
