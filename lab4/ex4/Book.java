package bozdean.catalin.lab4.ex4;

import bozdean.catalin.lab4.ex2.Author;

public class Book {
	static String name;
	static Author author;
	static double price;
	static int qtyInStock;
	
	public Book (String n, Author a, double p) {
		name=n;
		author=a;
		price=p;
	}
	
	public Book (String n, Author a, double p, int q) {
		name=n;
		author=a;
		price=p;
		qtyInStock=q;
	}
	
	public String getName(){
		return name;
	}
	
	public String getAuthor(){
		return author.toString();
	}
	
	public double getPrice(){
		return price;
	}
	
	public static void setPrice(double p){
		price=p;
	}
	
	public static int getQtyInStock(){
		return qtyInStock;
	}
	
	public static void setQtyInStock(int q){
		qtyInStock=q;
	}
	
	public String ToString(){
		return name+" by author "+author.toString()+"\n";
	}
}
