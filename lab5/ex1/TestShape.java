package bozdean.catalin.lab5.ex1;

import bozdean.catalin.lab5.ex1.Circle;
import bozdean.catalin.lab5.ex1.Rectangle;
import bozdean.catalin.lab5.ex1.Square;

public class TestShape {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Circle c = new Circle(3.12, "green", true);
		System.out.println(c.getArea());
		System.out.println(c.getPerimeter());
		System.out.println(c.toString());

		System.out.println();

		Rectangle r = new Rectangle(10.0, 12.0, "red", true);
		System.out.println(r.getArea());
		System.out.println(r.getPerimeter());
		System.out.println(r.toString());

        System.out.println();

        Square s1 = new Square(5.0, "purple", false);
        s1.setWidth(s1.side);
        s1.setLength(s1.side);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        System.out.println(s1.toString());

        System.out.println();
	}
}
