package bozdean.catalin.lab5.ex1;

public class Rectangle extends Shape{
	protected double width;
	protected double length;
	
	public Rectangle(){};
	
	public Rectangle(double w,double l){
		this.width=w;
		this.length=l;
	}
	
	public Rectangle(double w,double l,String c,boolean f){
		this.width=w;
		this.length=l;
		this.color=c;
		this.filled=f;
	}
	
	public double getWidth(){
		System.out.println("Rectangles width is: ");
		return this.width;
	}
	
	public void setWidth(double w){
		this.width=w;
	}
	
	public double getLength(){
		System.out.println("Rectangles length is: ");
		return this.length;
	}
	
	public void setLength(double l){
		this.length=l;
	}
	
	public double getArea(){
		System.out.println("Rectangles area is: ");
		return this.length*this.width;
	}
	
	public double getPerimeter(){
		System.out.println("Rectangles perimeter is: ");
		return 2*(this.width+this.length);
	}
	
	public String toString(){
		return "The Rectangle has:\nlength="+this.length+"\nwidth"+this.width+"\ncolor="+this.color+"\nAnd "+isFilled();
	}
}





