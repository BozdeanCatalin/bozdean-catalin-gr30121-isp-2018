package bozdean.catalin.lab5.ex1;

public class Square extends Rectangle{
	double side;
	public Square(){}
	
	public Square(double s){
		this.side=s;
	}
	
	public Square(double s,String c,boolean f){
		this.side=s;
		this.color=c;
		this.filled=f;
	}
	
	public double getSide(){
		System.out.println("Squares side is: ");
		return this.side;
	}
	
	public void setSide(double s){
		this.side=s;
	}
	
	public void setWidth(double s){
		this.width=s;
	}
	
	public void setLength(double s){
		this.length=s;
	}
	
	public String toString(){
		return "The Square has:\nside"+this.side+"\nlength="+this.length+"\nwidth"+this.width+"\ncolor="+this.color+"\nAnd "+isFilled();
	}
}




