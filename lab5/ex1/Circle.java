package bozdean.catalin.lab5.ex1;

public class Circle extends Shape{
	protected double radius;
	
	public Circle(){};
	
	public Circle(double r){
		this.radius=r;
	}
	
	public Circle(double r,String c,boolean f){
		this.radius=r;
		this.color=c;
		this.filled=f;
	}
	
	public double getRadius(){
		System.out.println("The Circle has radius= ");
		return this.radius;
	}
	
	public void setRadius(double r){
		this.radius=r;
	}
	
	public double getArea(){
		System.out.println("Circles area is: ");
		return Math.PI*radius*radius;
	}
	
	public double getPerimeter(){
		System.out.println("Circles perimeter is: ");
		return 2*Math.PI*radius;
	}
	
	public String toString(){
		return "The circle has:\nradius="+this.radius+"\ncolor="+this.color+"\nAnd "+isFilled();
	}
}





