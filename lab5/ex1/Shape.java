package bozdean.catalin.lab5.ex1;

public abstract class Shape {
	protected String color;
	protected boolean filled;
	
	public Shape() {}
	public Shape(String color,boolean filled){}
	
	public String getColor(){
		System.out.println("Color is:");
		return color;
	}
	
	public void setColor(String c){
		this.color=c;
	}
	
	public String isFilled(){
		if(filled)return "The Shape is filled";
		else return "The Shape is NOT filled";
	}
	
	public void setFilled(boolean f){
		this.filled=f;
	}
	
	public abstract double getArea();
	public abstract double getPerimeter();
	public abstract String toString();
	
	
}
	
	
	
	
	
	
	
	
	