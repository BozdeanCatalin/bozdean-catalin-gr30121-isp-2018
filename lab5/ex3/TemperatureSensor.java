package bozdean.catalin.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
	
	public int readValue(){
		Random rn= new Random();
		int tempSensor=rn.nextInt(100)+1;
		return tempSensor;
	}
}
