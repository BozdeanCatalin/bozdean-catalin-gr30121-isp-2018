package bozdean.catalin.lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor{
	public int readValue(){
		Random rn= new Random();
		int lightSensor=rn.nextInt(100)+1;
		return lightSensor;
	}
}
