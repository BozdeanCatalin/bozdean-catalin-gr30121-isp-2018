package bozdean.catalin.lab5.ex3;

public class Controller {
	
	public void control() throws InterruptedException {


        Sensor tempSensor = new TemperatureSensor();
        Sensor lightSensor = new LightSensor();

        int sec = 1;
        while (sec <=20)
        {
            System.out.println("Temp: " + tempSensor.readValue());
            System.out.println("Light: " + lightSensor.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }
	}
}