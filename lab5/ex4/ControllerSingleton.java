package bozdean.catalin.lab5.ex4;

import bozdean.catalin.lab5.ex3.LightSensor;
import bozdean.catalin.lab5.ex3.Sensor;
import bozdean.catalin.lab5.ex3.TemperatureSensor;

public class ControllerSingleton {
		private static ControllerSingleton controller;
		
		private ControllerSingleton(){};
		
		public void control() throws InterruptedException {


	        Sensor tempSensor = new TemperatureSensor();
	        Sensor lightSensor = new LightSensor();

	        int sec = 1;
	        while (sec <=20)
	        {
	            System.out.println("Temp: " + tempSensor.readValue());
	            System.out.println("Light: " + lightSensor.readValue());
	            System.out.println("Sec: " + sec);
	            sec++;
	            Thread.sleep(1000);
	        }
		}
		
		public static ControllerSingleton getController() {
	        if (controller == null) {
	            controller = new ControllerSingleton();
	        }
	        return controller;
		}
}
