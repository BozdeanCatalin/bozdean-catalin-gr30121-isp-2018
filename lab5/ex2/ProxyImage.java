package bozdean.catalin.lab5.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProxyImage implements Image{
	private RealImage realImage;
	private RotatedImage rotatedImage;
	   private String fileName;
	 
	   public ProxyImage(String fileName) throws IOException{
	      this.fileName = fileName;
	      System.out.println("Displaying Real or Rotated?");
	      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	      String n = br.readLine();
	      if(n=="Real")realImage.display();
	      else 
	    	  if(n=="Rotated")rotatedImage.display();
	   }
	 
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	      
	      if(rotatedImage == null){
		     rotatedImage = new RotatedImage(fileName);
		  }
	      rotatedImage.display();
	      //ProxyImage img=new ProxyImage(fileName);
	}
}
