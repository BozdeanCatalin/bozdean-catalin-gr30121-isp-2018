package bozdean.catalin.lab9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ButtonCounter extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextField tClick;
    JButton bCount;
    int x=0;
    
    public ButtonCounter(){
    	
    	setTitle("click counter");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }
    
    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        tClick = new JTextField("Click");
        tClick.setBounds(10, 50, width, height);

  }
    class TratareButonLoghin implements ActionListener{

        public void actionPerformed(ActionEvent e) {
        	ButtonCounter.this.x++;
        	ButtonCounter.this.tClick.setText(String.valueOf(x));
        }    
  }
}
