package bozdean.catalin.lab6.ex4;

import java.util.HashMap;

public class Dictionary {
	HashMap<Word, Definition> dct = new HashMap<Word, Definition>();

    public void addWord(Word w, Definition d) {
        if (dct.containsKey(w))
            System.out.println("Modify:");
        else
            System.out.println("New word:");
        dct.put(w, d);
    }

    public Object getDefinition(Word w) {
    	System.out.println("Searching for " + w);
    	System.out.println(dct.containsKey(w));
        return dct.get(w);
    }

    public void afisDictionar()

    {
    	System.out.println(dct);
    }

    public void getAllWords() {

        for (Object word : dct.keySet()) System.out.println(word.toString());
    }
    public void getAllDefinitions() {

        for (Object definition : dct.values()) System.out.println(definition.toString());
    }
}
