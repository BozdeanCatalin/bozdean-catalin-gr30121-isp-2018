package bozdean.catalin.lab6.ex3;

public class Test {
	public static void main(String[] args){
		Bank x=new Bank();
		x.addAccount("Ion",100);
		x.addAccount("Bob",110);
		x.addAccount("Andrei",80);
		x.addAccount("Ioana",150);
		x.addAccount("Ana",90);
		x.printAccounts();
		x.printAccounts(90,130);
		x.getAccount("Andrei",110);
		x.getAllAccount();
	}
}
