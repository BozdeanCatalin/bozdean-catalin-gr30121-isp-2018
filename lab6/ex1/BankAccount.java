package bozdean.catalin.lab6.ex1;

public class BankAccount {
	public String owner;
	public double balance;
	
	public BankAccount(String o,double b){
		this.setOwner(o);
		this.setBalance(b);
	}
	
	public BankAccount() {
		// TODO Auto-generated constructor stub
	}

	public void withdraw(double amount){
		System.out.println("You took: "+amount);
		this.setBalance(this.getBalance()-amount);
	}
	
	public void deposit(double amount){
		System.out.println("You introduced: "+amount);
		this.setBalance(this.getBalance()+amount);
	}
	
	public boolean equals(Object name){
		if(name instanceof BankAccount){
			BankAccount n=(BankAccount)name;
			return (this.getOwner()==n.getOwner() && this.getBalance()==n.getBalance());
		}
		else return false;
	}
	
	public int hashCode(){
		System.out.println("HashCode is");
		return this.getOwner().hashCode()+(int)this.getBalance()+1;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
}




