package bozdean.catalin.lab6.ex2;

import java.util.ArrayList;
import bozdean.catalin.lab6.ex1.BankAccount;

public class Bank {
	ArrayList<BankAccount> accounts=new ArrayList<BankAccount>();
	
	public void addAccount(String owner,double balance){
		BankAccount x=new BankAccount(owner,balance);
		accounts.add(x);
	}
	
	
	
	public void printAccounts(){
		BankAccount prt=new BankAccount();
		for(int i=0;i<accounts.size();i++){
			prt=(BankAccount)accounts.get(i);
			System.out.println("Account with owner "+prt.getOwner()+" have: "+prt.getBalance());
			}    
	}
	
	public void printAccounts(double minbalance,double maxbalance){
		BankAccount prt=new BankAccount();
		for(int i=0;i<accounts.size();i++){
			prt=(BankAccount)accounts.get(i);
			if(prt.getBalance()>minbalance && prt.getBalance()<maxbalance)
				System.out.println("Account with owner "+prt.getOwner()+" have balance: "+prt.getBalance());
		}    
	}
	
	BankAccount getAccount(String owner){
		BankAccount x=new BankAccount();
		for(int i=0;i<accounts.size();i++){
			x=(BankAccount)accounts.get(i);
			if(x.getOwner()==owner)
				return x;
		}
		return null;
	}
	public BankAccount getAllAccounts()
    {
        int sortat;
        do {
            sortat = 1;
            for (int i = 0; i < accounts.size() - 1; i++) {
                BankAccount b1 = (BankAccount) accounts.get(i);
                BankAccount b2 = (BankAccount) accounts.get(i + 1);
                BankAccount b3 = new BankAccount(" ", 0);

                if (b1.owner.compareTo(b2.owner)>0) {
                	b3.owner = b1.owner;
                    b3.balance = b1.balance;
                    b1.owner = b2.owner;
                    b1.balance = b2.balance;
                    b2.owner = b3.owner;
                    b2.balance = b3.balance;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        for (int i = 0; i < accounts.size(); i++) {
            BankAccount b = (BankAccount) accounts.get(i);
            System.out.println(b.owner + " " + b.balance);
        }
        return null;
    }		
}





