package bozdean.catalin.lab7.ex4;

import java.io.Serializable;

public class Car implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String model;
    private float price;

    public Car(String model,float price){
        this.model=model;
        this.price=price;

    }

    public String getModel() {
        return model;
    }

    public float getPrice() {
        return price;
    }
}
