package bozdean.catalin.lab2.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrimeNumbers {
	private static void PrimeNumbers(int a,int b){
		if(a>b){
			int aux=a;
			a=b;
			b=aux;
		}
		int i, nr=a, x=0,ok ;
		while (nr<=b){
			ok=1;
			for(i=2;i<(nr/2+1);i++){
				if(nr%i==0)ok=0;
			}
			if(ok==1){
				System.out.print(nr+" ");
				x++;
			}
			nr++;
		}
		System.out.print("we have "+x+" prime numbers");
	}

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("a=");
        int a = Integer.parseInt(br.readLine());
        System.out.print("b=");
        int b = Integer.parseInt(br.readLine());
        PrimeNumbers(a,b);
	}
}
