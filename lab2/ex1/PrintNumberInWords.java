package bozdean.catalin.lab2.ex1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrintNumberInWords {
	private static void PrintNumberInWords(int x) {
		// TODO Auto-generated method stub
		switch(x){
		case(0):System.out.print("zero");break;
		case(1):System.out.print("one");break;
		case(2):System.out.print("two");break;
		case(3):System.out.print("three");break;
		case(4):System.out.print("four");break;
		case(5):System.out.print("five");break;
		case(6):System.out.print("six");break;
		case(7):System.out.print("seven");break;
		case(8):System.out.print("eight");break;
		case(9):System.out.print("nine");break;
		default:System.out.print("other");break;
		}
	}
	
	public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter Integer:");
        int x = Integer.parseInt(br.readLine());
        PrintNumberInWords(x);
	}
}
