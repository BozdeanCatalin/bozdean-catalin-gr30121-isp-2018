package bozdean.catalin.lab2.ex7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class GuessNumber {
	public static int t=0;
	private static void GuessNumber() throws IOException{
		int m;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("x=");
        int x = Integer.parseInt(br.readLine());
		Random rn= new Random();
		m=rn.nextInt(10)+1;
		if(x==m){
			System.out.print("You guessed it");
		}
		else if(t==3)System.out.print("Game Over");
			else {
				
				System.out.print("Try again");
				t++;
				GuessNumber();
		}
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		GuessNumber();
	}

}
