package bozdean.catalin.lab2.ex6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Factorial {
	private static void Factorial(int n){
		int i,x=1;
		for(i=1;i<=n;i++)x=x*i;
		System.out.print("n!= "+x);
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("n=");
        int n = Integer.parseInt(br.readLine());
        Factorial(n);
	}

}
